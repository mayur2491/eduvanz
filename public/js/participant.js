$(function () {
  alldata();
    
});

function alldata(){
    $.ajax({url: "api/list", 
        type:"get",
        success: function(result){
               
          var data='';
          $.each(result.participants, function(k, each_row) {
            data +='<tr id=row'+each_row.id+'><td>'+each_row.name+'</td><td>'+each_row.age+'</td><td>'+each_row.dob+'</td><td>'+each_row.profession+'</td><td>'+each_row.locality+'</td><td>'+each_row.no_of_guest+'</td><td>'+each_row.address+'</td></tr>';
     
          });

          $("#userdata").html(data);
          $('#usertable').DataTable({
            "pageLength": 5
          });
    }
  });
  }
<?php

namespace App\Http\Controllers;


use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Participant;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


class ParticipantController extends Controller
{

	public function register(Request $request){
	
			
			 $validator=Validator::make($request->all(), [
			    'name'=>'required',
            	'age'=>'required|numeric',
            	'dob'=>'required|date',
            	'profession'=>['required',Rule::in(['Employed', 'Student']),],
            	'locality'=>'required',
            	'no_of_guest'=>'required|numeric|min:0|max:2',
            	'address'=>'required|max:50'
			]);
			if ($validator->fails()) {
				return response(['ResponseStatus'=>0,'Errors'=>$validator->errors()]);
			}

		try{

	 		$participant=new Participant($request->all());
	        $result=$participant->save();
	        if($result){
	            return response(['Message'=>'Data Save Sucessfully','ResponseStatus'=>1]);
	        }else{
	            return response(['Message'=>'Data saving failed','ResponseStatus'=>0]);
	        }

		}catch(\Exception $e){

			return response(['Message'=>$e->getMessage().'-'.$e->getLine(),'ResponseStatus'=>0]);
		}
		
    	
    }

    public function list(){
    	$participants=Participant::all();   
    	return response(['participants'=>$participants]);
    }

    public function update(Request $request,$id){
    	 $validator=Validator::make($request->all(), [
			    'name'=>'required',
            	'age'=>'required|numeric',
            	'dob'=>'required|date',
            	'profession'=>['required',Rule::in(['Employed', 'Student']),],
            	'locality'=>'required',
            	'no_of_guest'=>'required|numeric|min:0|max:2',
            	'address'=>'required|max:50'
			]);
			if ($validator->fails()) {
				return response(['ResponseStatus'=>0,'Errors'=>$validator->errors()]);
			}

    	try{
    		
    		$participant=Participant::findOrFail($id);
			$result=$participant->update($request->all());
		        
	        if($result){
	            return response(['Message'=>'Data updated sucessfully','ResponseStatus'=>1,'Data'=>$participant]);
	        }else{
	            return response(['Message'=>'Data updation failed','ResponseStatus'=>0]);
	        }

    	}catch(\Exception $e){
			return response(['Message'=>$e->getMessage().'-'.$e->getLine(),'ResponseStatus'=>0]);
    	}
                
   }

}
<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Exception;
use DB;
class Participant extends Model
{
	use SoftDeletes;
	protected $connection = 'mysql';
	protected $table='participants';
	
	protected $fillable = [
        'name', 'age','dob','profession','locality','no_of_guest','address','deleted_at','created_at','updated_at'
    ];
    
}

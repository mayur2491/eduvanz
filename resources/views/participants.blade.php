<!DOCTYPE html>
<html>
<head>
  
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
   <link rel="stylesheet" href="http://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
  
</head>
<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">

@php
$rowcount = 0;
@endphp


<table id="usertable" class=" table table-bordered table-striped" cellspacing="0" width="100%"> 
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Age</th>
                    <th>DOB</th>
                    <th>Profession</th>
                    <th>Locality</th>
                    <th>Number of Guests</th>
                    <th>Address</th>
                    
                </tr>
                </thead>
                <tbody id="userdata">
                    
                    
                </tbody>
            </table>
            
            
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>

<script src="{{URL::asset('js/participant.js')}}"></script>
</body>
</html>